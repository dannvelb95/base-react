import React from 'react'
import { TaskItem } from './'

const ContainerTasks = props => {
    const { items, theme } = props
    const getItems = () => 
        items.map((item, index) => 
                <TaskItem theme={theme} item={item} setItem={props.setItem} toggleInfo={props.toggleInfo} />
            
        )
    return <div style={theme.column}>
      <p style={theme.titleColumn}>{props.title}</p>
      <div>
        {getItems()}
      </div>
    </div>
  }
  
  export default ContainerTasks