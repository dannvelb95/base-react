import React, { Component } from 'react'

class TaskInfo extends Component {
    render = () => {
        const { theme,item } = this.props
        return <div style={theme.modalContainer}>
            <div style={theme.modal}>
                <h3 >Task Information</h3>
                <p style={theme.titleModal}>name: {item.name}</p>
                <p style={theme.descriptionModal}>description: {item.description}</p>
                <p style={theme.status}>status: {item.status.description}</p>
                <button style={theme.buttonCancel} onClick={this.props.hide}>close</button>
            </div>
        </div>
    }
}


export default TaskInfo
