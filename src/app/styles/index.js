const colors = {
    orange: '#BE7C4D',
    black: '#141414',
    white: '#EEF0F2',
    blue: '#0E1116',
    white2: '#FAFFFD',
    purple: '#5F5AA2'
}

const basetheme = {
    all: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        background:colors.white
    }, header: {
        padding: '30px 10px',
    }, body: {
        display: 'flex',
        width: '100%',
        bottom: 0
    }
}

export const theme = {
    ...basetheme,
    buttonCancel: {
        width: 70,
        background: 'none',
        border: 'none',
        fontSize: 8,
        borderRadius: 5,
        textAlign: 'right',
        padding: '2px 5px',
        color: 'red',
        cursor: 'pointer',
        float: 'right'
    }, buttonSubmit: {

    }, titleModal: {
        fontSize: 16,

    }, descriptionModal: {
        fontSize: 10,
        margin: 'auto'
    }, status: {

    }, modal: {
        margin: 'auto',
        background: colors.white2,
        width: '60%',
        padding: 15,
        borderWidth: 4,
        borderStyle: 'solid',
        borderColor: colors.purple,
        borderRadius: 2,
    }, modalContainer: {
        position: 'fixed',
        width: '100%',
        height: '100%',
        top: 0,
        background: 'rgba(1,1,1,.5)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    status: {
        color: 'green',
        fontSize: 10
    },
    titleItem: {
        fontSize: 10

    },
    subtitleItem: {
        fontSize: 7

    }, statusItem: {
        fontSize: 7

    }, containerItem: {
        background: colors.purple,
        color: '#ffff',
        padding: '1px 6px',
        margin: '9px 0px',
        borderRadius: '5px'
    }, column: {
        width: '25%',
        margin: ' 3px',
        borderWidth: 1,
        borderRadius:5,
        borderColor:colors.purple,
        borderStyle: 'solid',
        padding: '0px 5px',
        maxWidth: 200
    },
    titleColumn:{ 
        fontSize: 12 
    }
}

export const theme2 = {
    ...basetheme,
    buttonCancel: {
        width: 70,
        background: 'none',
        border: 'none',
        fontSize: 8,
        borderRadius: 5,
        textAlign: 'right',
        padding: '2px 5px',
        color: 'red',
        cursor: 'pointer',
        float: 'right'
    }, buttonSubmit: {

    }, titleModal: {
        fontSize: 16,

    }, descriptionModal: {
        fontSize: 10,
        margin: 'auto'
    }, status: {

    }, modal: {
        margin: 'auto',
        background: colors.white2,
        width: '60%',
        padding: 15,
        borderWidth: 4,
        borderStyle: 'solid',
        borderColor: colors.orange,
        borderRadius: 2,
    }, modalContainer: {
        position: 'fixed',
        width: '100%',
        height: '100%',
        top: 0,
        background: 'rgba(1,1,1,.5)',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    status: {
        color: 'green',
        fontSize: 10
    },
    titleItem: {
        fontSize: 10

    },
    subtitleItem: {
        fontSize: 7

    }, statusItem: {
        fontSize: 7

    }, containerItem: {
        background: colors.blue,
        color: '#ffff',
        padding: '1px 6px',
        margin: '9px 0px',
        borderRadius: '5px'
    }, column: {
        width: '25%',
        margin: ' 3px',
        borderWidth: 1,
        borderRadius:5,
        borderColor:colors.orange,
        borderStyle: 'solid',
        padding: '0px 5px',
        maxWidth: 200
    },
    titleColumn:{ 
        fontSize: 12 
    }
}

