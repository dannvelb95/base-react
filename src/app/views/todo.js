import React, { createContext, Component } from 'react';
import tasks from '../../sample/tasks'
import { ContainerTasks, TaskInfo } from '../components'
import { theme as themeFile, theme2 } from '../styles/index'

class App extends Component {
    constructor() {
        super()
        this.state = {
            theme:theme2,
            showInfo: false,
            itemSelect: {
                name: 'Task one',
                description: 'description',
                status: {
                    type: 0,
                    description: 'to do'
                }
            }
        }
    }
    changeTheme = ()=>{
        this.setState({
            theme:this.state.theme==theme2?themeFile:theme2
        })
    }
    toggleInfo = () => 
        this.setState({ showInfo: !this.state.showInfo })

    setItem = (item) =>
        this.setState({ itemSelect: item })


    render = () => {
        const { theme } = this.state
        let todo = tasks.filter(item => item.status.type == 0)
        let inprogress = tasks.filter(item => item.status.type == 1)
        let done = tasks.filter(item => item.status.type == 2)
        return (
            <div style={theme.all}>
                <div style={theme.header}>
                    <p style={theme.titleModal}>React - react-redux - react-saga</p>
                    <button style={theme.buttonCancel} onClick={this.changeTheme}>Cambiar tema</button>
                </div>
                <div style={theme.body} className="App">
                    <ContainerTasks
                        theme={theme}
                        title='To do'
                        items={todo}
                        setItem={this.setItem}
                        toggleInfo={this.toggleInfo}/>
                    <ContainerTasks
                        theme={theme}
                        title='In progress'
                        items={inprogress}
                        setItem={this.setItem}
                        toggleInfo={this.toggleInfo} />
                    <ContainerTasks
                        theme={theme}
                        title='Ready for Test'
                        items={done}
                        setItem={this.setItem}
                        toggleInfo={this.toggleInfo} />
                    <ContainerTasks
                        theme={theme}
                        title='Done'
                        items={done}
                        setItem={this.setItem}
                        toggleInfo={this.toggleInfo} />
                </div>
                {this.state.showInfo ? <TaskInfo 
                        theme={theme}
                        item={this.state.itemSelect} 
                        hide={this.toggleInfo} /> : null}
            </div>
        );
    }
}


export default App;
